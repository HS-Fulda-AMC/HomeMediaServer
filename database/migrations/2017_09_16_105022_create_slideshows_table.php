<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlideshowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::create('slideshows', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
//            $table->string('slug')->unique();
            $table->integer('occasion_id')->unsigned();
            //$table->integer('order')->unsigned();
//            $table->string('occasionSlug')->unique();
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::dropIfExists('slideshows');
    }
}
