## About HomeMediaServer

HomeMediaServer ist ein Projekt im Rahmen des Moduls "Advanced Mutlimedia Communications" bei Prof. Dr. Sebsatian Rieger.
Es bietet ein Interface um für einen Anlass Vorschaubilder zu erstellen welche anschließend für eine Slideshow übernommen werden können.
Für die ausgewählten Bilder können Zeiten für die Dauer der Anzeige festgelegt werden.
Anschließend wird mit ffmpeg das Slideshow-Video erstellt

## Voraussetzungen
- Webserver mit Pretty-URLs 
- PHP 7 + GD-Bibliothek
- ffmpeg (mit libx264 + libx265)

## Installation
- Website-Root muss auf den Ordner <i>public</i> zeigen
- Der Projektinhalt kann über eine neue Installation von Laravel 5.4 kopiert werden.
- Das Projekt erwartet unter dem Verzeichnis <i>storage/app/public</i> einen <i>pictures</i> Ordner und darunter eine Ordnerstruktur nach dem Format: {Jahr}/{Anlass}/*{Bilder}
- Unter dem Verzeichnis <i>storage/app/public</i> wird weiterhin ein ordner <i>thumbs</i> erwartet in welchem die Vorschaubilder gespeichert werden.
- Im Verzeichnis <i>storage/app</i> wird ein Ordner <i>slideshows</i> erwartet in welchem alle temporären Dateien sowie die fertigen Videos für die jeweilige Slideshow gespeichert werden.
- Nach der Installation ist der Befehl <i>php artisan storage:link</i> im Wurzelverzeichnis der Laravel-Installation auszuführen.

## Verwendung
Der Code dieses Projektes darf jederzeit verwendet werden und wird im aktuellen Zustand bereitgestellt. Es werden keine Garantien oder Versprechen gegeben.