@extends('layouts.app')

@section('content')
    <div class="container" >
        <div class="row" >
            <div class="panel panel-default" >
                <div class="panel-heading" >Ordner Auswahl</div >
                <div class="panel-body" >
                    <ul class="list-group" >
                        @forelse ($dirs as $dir)
                            <li class="list-group-item" ><a href="\f\{{ $dir['name'] }}" >{{ $dir['name'] }}</a >
                            </li >
                        @empty
                            <p >Keine Verzeichnisse gefunden. Vermutlich ist der Netzwerkspeicher nicht zugreifbar. Eventuell hilft ein Server reboot.</p >
                        @endforelse
                    </ul >
                </div >
            </div >
        </div >
    </div >
@endsection
