@if(\Illuminate\Support\Facades\Session::has('flash_error_message'))
    <div class="form-group" >
        <div class="flash-error-message alert alert-danger {{        \Illuminate\Support\Facades\Session::has('flash_error_message_important') ? 'alert-important':''        }}" >
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button >
            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> {{ session('flash_error_message') }}
        </div >
    </div >
@endif
