<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class AddStatusToSlideshows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::table('slideshows', function ($table)
        {
            $table->enum('status', [ 'erstellen', 'bearbeiten', 'erstellt' ]);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        Schema::table('slideshows', function ($table)
        {
            $table->dropColumn('status');
        });
    }
}
