<?php

namespace App\Http\Controllers;

use App\Picture;
use App\Slideshow;
use Illuminate\Http\Request;
use Storage;


class SlideshowsController extends Controller
{
    /**
     * Show slideshows for an occasion and handles to work(modify/reset/delete) with them
     *
     * @param $occasion
     *
     * @return $this
     */
    public function index ($occasion)
    {
        //dd($occasion->slideshows()->with('pictures')->get(), $occasion);
        //if($occasion->slideshows()->exists())
        $slideshows = $occasion->slideshows()->with('pictures')->get();
        
        return view('slideshow.index', compact('occasion', 'slideshows'))->with('flash_message', 'dummy');
    }
    
    /**
     * Create a slideshow and be able to select and order pictures to use
     *
     * @param $occasion
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create ($occasion)
    {
        $numSlideshows = count($occasion->slideshows()->get()) + 1;
        $slide         = NULL;
        if ($numSlideshows > 0
            && $occasion->slideshows()->latest()->exists()
            && !$occasion->slideshows()->latest()->first()->pictures()->exists())
        {
            $slide = $occasion->slideshows()->latest()->first();
        } else
        {
            $slide = $occasion->slideshows()->create([ 'name' => $numSlideshows ]);
        }
        $picsAvail              = $occasion->pictures()->get();
        $placeholder            = new Picture();
        $placeholder->name      = "300x150.png";
        $placeholder->thumbname = $placeholder->name;
        $placeholder->ppath     = storage_path('app/public') . "/pictures/dummy";
        $picsUsed               = collect([ $placeholder ]);

        return view('slideshow.create', compact('occasion', 'slide', 'picsAvail', 'picsUsed'));
    }
    
    /**
     * Edit the selected pictures and order of a slideshow
     *
     * @param $occasion
     * @param $slide
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit ($occasion, $slide)
    {
        $picsUsed  = $occasion->pictures()->whereIn('id', $slide->pictures()->select('picture_id')->get())->get();
        $picsAvail = $occasion->pictures()->whereNotIn('id', $slide->pictures()->select('picture_id')->get())->get();

        return view('slideshow.create', compact('occasion', 'slide', 'picsAvail', 'picsUsed'));
    }
    
    /**
     * Store modified comments (if set) and duration for pictures in slideshow
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $occasion
     * @param                          $slide
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeTime (Request $request, $occasion, $slide)
    {
        if (isset($request->id, $request->comment, $request->duration))
        {
            $pics = $slide->pictures()->get();
            foreach ($request->id as $key => $id)
            {
                $pic = $pics->where('id', $id)->first();
                if ( !is_null($request->comment[$key]))
                    $pic->comment = $request->comment[$key];
                $pic->duration = ( ( is_numeric($request->duration[$key]) && 1 <= $request->duration[$key] &&
                                     $request->duration[$key] <= 30 ) ? $request->duration[$key] : 4 );
                $pic->save();
            }
        }
        
        return redirect()
            ->action('SlideshowsController@index', [ 'occasion' => $occasion->slug ])
            ->with('flash_message', 'Zeiten und Kommentare für Slideshow ' . $slide->name . ' gespeichert.');
    }
    
    /**
     * Load Slideshow and its pictures to modify duration and comment for the slideshow
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $occasion
     * @param                          $slide
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function time (Request $request, $occasion, $slide)
    {
        if ($slide->pictures()->exists())
        {
            $slide->status = "bearbeiten";
            $slide->save();
        } else
        {
            return redirect()
                ->action('SlideshowsController@index', [ 'occasion' => $occasion->slug ])
                ->with('flash_error_message', 'Slideshow ' . $slide->name .
                                              ' hat noch keine Bilder zugewiesen. Bearbeitungsschritt noch nicht möglich.');
        }
        
        return view('slideshow.time', compact('occasion', 'slide'));
    }
    
    public function resetslide ($occasion, $slide)
    {
        $this->resetHandler($occasion, $slide);
        
        return redirect()
            ->action('SlideshowsController@generate', [ 'occasion' => $occasion->slug, 'slideshow' => $slide->id ])
            ->with('flash_message', 'Generierte Slideshow ' . $slide->name . ' zurückgesetzt.');;
    }
    
    public function resetHandler ($occasion, $slide)
    {
        $path = $slide->getStoragepath() . '/' . $occasion->slug;
        Storage::delete(collect(Storage::files($path))->filter(function ($item) use ($path, $slide)
        {
            return substr($item, strrpos($item, $slide->name), strlen($slide->name) + 1) == $slide->name . "-";
        })->all());
        
        $slide->generated = false;
        $slide->save();
    }
    
    public function generate ($occasion, $slide)
    {
        $slide->status = "erstellt";
        $filename      = $slide->getFilename();
        $filenameSlide = $filename . '-.config';
        $filepath      = $slide->getStoragepath() . '/' . $filenameSlide;
        $slidepath     = $slide->getPath() . '/' . $slide->getFilename();
        $stdout        = $slidepath . '-.stdout';
        $stderr        = $slidepath . '-.stderr';
        Storage::put($filepath, "empty"); // create file so directory exists on slideshow picture creation
        if ( !$slide->generated)
        {
            $file      = "";
            $slidepics = $slide->pictures()->with('picture')->orderBy('order')->get();
            $w         = 0;
            $h         = 0;
            for ($i = 0; $i < count($slidepics); $i++)
            {
                list($width, $height) = getimagesize($slidepics->get($i)->picture->ppath);
                if ($width > $w)
                {
                    $w = $width;
                }
            }
            if ($w > 1600)
            {
                $w = 1920;
                $h = 1080;
            } else
                if ($w > 1360)
                {
                    $w = 1600;
                    $h = 900;
                } else
                    if ($w > 1280)
                    {
                        $w = 1360;
                        $h = 765;
                    } else
                        if ($w > 800)
                        {
                            $w = 1280;
                            $h = 720;
                        } else
                            if ($w > 768)
                            {
                                $w = 800;
                                $h = 450;
                            } else
                                if ($w > 640)
                                {
                                    $w = 768;
                                    $h = 432;
                                } else
                                {
                                    $w = 640;
                                    $h = 360;
                                }
            $picname = "";
            for ($i = 0; $i < count($slidepics); $i++)
            {
                set_time_limit(50);
                $slidepic = $slidepics->get($i);
                $picname  = str_pad($i, 3, '0', STR_PAD_LEFT) . '.png';
                $to = $slidepath . '-' . $picname;
                $slidepic->picture->createSlideshowPicture($to, $w, $h);
                $file .= "file '" . $slide->name . '-' . $picname . "'\n";
                $file .= 'duration ' . $slidepic->duration . "\n";
            }
            $command =
                'ffmpeg -y -f concat -i {filename} -c copy {slidename}-.temp.mp4 >{stdout}.tmp 2>{stderr}.tmp';
            $file    .= "file '" . $slide->name . '-' . $picname . "''\n";
            $command = str_replace(array( '{filename}', '{slidename}', '{stdout}', '{stderr}' ),
                                   [ $slide->getPath() . '/' . $filenameSlide,
                                     $slide->getPath() . '/' . $filename,
                                     $stdout,
                                     $stderr ], $command);
            Storage::put($filepath, $file);
            
            set_time_limit(30);
            shell_exec($command);
// -movflags +faststart // moves important information to start of file so playback can start before loading the whole thing
            $command2 =
                'ffmpeg -i {slidename}-.temp.mp4 -pix_fmt yuv420p -c:v libx264 -preset slow -an -y {slidename}-.264.mp4 >{stdout} 2>{stderr} ';
            $command2 =
                str_replace([ '{slidename}', '{stdout}', '{stderr}' ],
                            [ $slide->getPath() . '/' . $filename, $stdout, $stderr ], $command2);
            set_time_limit(30);
            shell_exec($command2);
            $command3 =
                'ffmpeg -i {slidename}-.temp.mp4 -pix_fmt yuv420p -c:v libx265 -preset slow -an -y {slidename}-.265.mp4 >>{stdout} 2>>{stderr} ';
            $command3 =
                str_replace([ '{slidename}', '{stdout}', '{stderr}' ],
                            [ $slide->getPath() . '/' . $filename, $stdout, $stderr ], $command3);
            set_time_limit(30);
            shell_exec($command3);
            sleep(2);
            $slide->generated = true;
        }
        $ffmpeg_output = "Datei noch nicht geschrieben. Videoerstellung vermutlich nicht abgeschlossen!";
        $stderr        = $slide->getStoragepath() . '/' . $filename . '-.stderr';
        if (Storage::exists($stderr) && Storage::exists($stderr . '.tmp'))
            $ffmpeg_output = "Bilder zu Slideshow: \n\n" . Storage::get($stderr . '.tmp') .
                             "\n\nPixelformat der Slideshow ändern:\n\n" . Storage::get($stderr);
        else
        {
            //dump($stderr, Storage::exists($stderr));
        }
        $slide->save();
        
        return view('slideshow.generate', compact('occasion', 'slide', 'ffmpeg_output'));
    }
    
    public function getVideo ($slide)
    {
        $filename = $slide->getStoragepath() . '/' . $slide->getFilename() . '-.264.mp4';
        $contents = "";
        if (Storage::exists($filename))
        {
            $filename = $slide->getPath() . '/' . $slide->getFilename() . '-.264.mp4';
            
            return response()->file($filename);
            $contents = Storage::get($filename);
        } else
            abort(404);
        $response = \Response::make($contents, 200);
        $response->header('Content-Type', "video/mp4");
        
        return $response;
    }
    
    /**
     * Save changed Slideshow Name
     *
     * @param \Illuminate\Http\Request $request
     * @param                          $occasion
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store (Request $request, $occasion)
    {
        $slide = Slideshow::find($request->id);
        if (isset($request->id, $request->name) AND is_numeric($request->id) AND strlen($request->name) >= 1)
        {
            $slide->name = $request->name;
            $slide->save();
        }
        
        return redirect()
            ->action('SlideshowsController@index', [ 'occasion' => $occasion->slug ])
            ->with('flash_message', 'Änderung an Slideshow gespeichert');
    }
    
    /**
     * Used for Ajax processing
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update (Request $request)
    {
        if (is_numeric($request->slide))
        {
            $slide = Slideshow::find($request->slide);
            if ($request->action == "content")
            {
                $pictureIdToChange = $request->changedid;
                //dump($request);
                if ($request->added)
                {
                    // add
                    $slide->pictures()->create([ 'picture_id' => $pictureIdToChange, 'order' => 0, 'duration' => 4 ]);
                    $slide->saveOrder($request);
                } else
                {
                    // remove
                    //dd($pictureIdToChange,
                    $slide->pictures()->where('picture_id', $pictureIdToChange)->first()->delete();
                }
            } else if ($request->action == "change")
            {
                //dump($request);
                $slide->saveOrder($request);
            } else
            {
                return response('Invalid Action', 400);
            }
        } else
        {
            return response('Invalid Request Format', 400);
        }
        
        return response('Update Successful', 200);
    }
    
    /**
     * Reset a slideshows status (selected pictures and picture settings are currently kept)
     *
     * @param $occasion
     * @param $slide
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset ($occasion, $slide)
    {
        $slide->status = "erstellen";
        $this->resetHandler($occasion, $slide);
        
        return redirect()
            ->action('SlideshowsController@index', [ 'occasion' => $occasion->slug ])
            ->with('flash_message', 'Slideshow ' . $slide->name . ' wurde zurückgesetzt');
    }
    
    /**
     * Delete the selected slideshow
     *
     * @param $occasion
     * @param $slide
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete ($occasion, $slide)
    {
        $this->resetHandler($occasion, $slide);
        $slide->delete();
        
        return redirect()
            ->action('SlideshowsController@index', [ 'occasion' => $occasion->slug ])
            ->with('flash_message', 'Slideshow ' . $slide->name . ' wurde gelöscht!');
    }
}
