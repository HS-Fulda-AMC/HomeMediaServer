<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >
<head >
    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1" >

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" >

    <title >{{ config('app.name', 'Laravel') }}</title >
    @include('layouts.imports')
</head >
<body >
<div id="app" >
    <nav class="navbar navbar-default navbar-static-top" >
        <div class="container" >
            <div class="navbar-header" >

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}" >
                    {{ config('app.name', 'Laravel') }}
                </a >
            </div >

            <div class="collapse navbar-collapse" id="app-navbar-collapse" >
                @include('layouts.nav')
                @include('layouts.login')
            </div >
        </div >
    </nav >
    <div class="container" >
        @include('layouts.info')
        @include('layouts.error-flash')
        @yield('content')
        @include('layouts.errors')
    </div >
</div >
@include('cookieConsent::index')
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" ></script >
@if(\Illuminate\Support\Facades\Session::has('flash_error_message'))
    <script >
        $('div.flash-error-message').not('.alert-important').delay(5000).slideUp(700);
    </script >
@endif
@if(\Illuminate\Support\Facades\Session::has('flash_message'))
    <script >
        $('div.flash-message').not('.alert-important').delay(5000).slideUp(700);
    </script >
@endif
</body >
</html >
