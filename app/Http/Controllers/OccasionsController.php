<?php

namespace App\Http\Controllers;

use App\Occasion;
use Illuminate\Support\Facades\Storage;

class OccasionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct ()
    {
        //$this->middleware('auth');
    }
    
    public function index ()
    {
        $name    = "world";
        $folders = Storage::disk('local')->directories('public/pictures');
        for ($i = 0; $i < sizeof($folders); $i++)
        {
            $str         = $folders[$i]; // complete path in storage
            $tmp         = explode("/", $str); // last folder name (relevant)
            $folders[$i] = [ 'link' => $str, 'name' => end($tmp) ];//link not required
        }
        return view('pics.root')->with([ 'name' => $name, 'dirs' => $folders ]);
    }
    
    public function rootFolder ()
    {
        $name    = "world";
        $folders = Storage::disk('local')->directories('public/pictures');
        for ($i = 0; $i < sizeof($folders); $i++)
        {
            $str         = $folders[$i]; // complete path in storage
            $tmp         = explode("/", $str); // last folder name (relevant)
            $folders[$i] = [ 'link' => $str, 'name' => end($tmp) ];//link not required
        }
        
        //dd($folders);
        return view('home')->with([ 'name' => $name, 'dirs' => $folders ]);
    }
    
    public function folder ($folder)
    {
        $occasions = Storage::disk('local')->directories('public/pictures/' . $folder);
        for ($i = 0; $i < sizeof($occasions); $i++)
        {
            $tmp           = explode("/", $occasions[$i]);
            $occasions[$i] = end($tmp);
        }
        
        return view('pics.folder', compact('occasions', 'folder'));
    }
    
    public function occasion ($folder, $occ)
    {
        $allowedFileExtensions = array( 'png', 'jpg', 'svg', 'bmp' );
        // get files of chosen occasion
        $tmp = Storage::disk('local')->files('public/pictures/' . $folder . '/' . $occ);
        // found something?
        $occasion = NULL;
        if (count($tmp) > 1)
        {
            // already in database?
            $occasion = Occasion::where([ 'root_folder' => $folder, 'name' => $occ ])->first();
            if (count($occasion) == 0)
            {
                // no? create!
                $occasion = Occasion::create([ 'name'        => $occ,
                                               'root_folder' => $folder,
                                               'path'        => 'public/pictures/' ]);
            }
            foreach ($tmp AS $key => $val)
            {
                // remove items with wrong file extension
                $val = strtolower(pathinfo($val)['extension']);
                if ( !in_array($val, $allowedFileExtensions))
                    unset($tmp[$key]);
                else
                {
                    // load known pictures to prevent doing the same work twice
                    $knownPictures = array_flatten($occasion->pictures()// known pictures
                                                            ->get([ "name" ])// just the name attribute
                                                            ->toArray()); // cast to array
                    if ( !is_numeric(array_search(pathinfo($tmp[$key])['basename'], $knownPictures)))
                    {
                        // store picture to database
                        $occasion->addPicture(pathinfo($tmp[$key])['basename'], $tmp[$key]);
                    }
                    
                }
            }
        } else
        {
            //dd($folder, $occ);
            return redirect()->back()->withErrors([ "pictures" => "Keine Bilder (für " . $folder . "/" . $occ .
                                                                  ") gefunden!" ]);
            //redirect('/f/'.$folder);
        }
        
        //dd($folder, $occ, collect($tmp), $tmp, pathinfo($tmp[0]), storage_path('app'), $occasion);
        //dump($occasion);
        
        return view('pics.occasion', compact('occasion'));
    }
}
