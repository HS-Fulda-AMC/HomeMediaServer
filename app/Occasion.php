<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Occasion extends Model
{
    use Sluggable;
    protected $fillable     = [ 'name', 'root_folder', 'path' ];
    protected $imgs_orig    = array();
    protected $imgs_cropped = array();
    
    /*
    public function registerMediaConversions ()
    {
        $this->addMediaConversion('thumb')
             ->width(368)
             ->height(232)
             ->sharpen(10)
             ->optimize();
    }
    
    public function loadMediaLibrary ($path)
    {
        
        foreach (Storage::disk('media')->files($path) AS $file)
        {
            //$imgs_o
            
            //$this->addMedia(Storage::disk('media')->getAdapter()->getPathPrefix().$file)->preservingOriginal()->toMediaCollection();
            dd(Storage::disk('media')->getAdapter()->getPathPrefix() . $file, $path, $file, $this, $path);
        }
        //dd($this->getMedia());
    }
    */
    
    public function sluggable ()
    {
        return [
            'slug' => [
                'source' => [ 'root_folder', 'name', ],
            ],
        ];
    }
    
    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName ()
    {
        return 'slug';
    }
    
    public function getRootFolder ()
    {
        return $this->root_folder;
    }
    
    public function addPicture ($name, $filepath)
    {
        $thumbname = $this->getThumbFilename($filepath);
        //dump($this,$this->pictures()->create(compact('name', 'thumbname')));
        $pic = $this->pictures()->create(compact('name', 'thumbname'));
        $pic->createThumb($filepath);
    }
    
    private function getThumbFilename ($path)
    {
        $tmp = array_slice(explode('/', $path), -3);
        
        return str_replace(" ", "-", implode("-", $tmp));
    }
    
    public function pictures ()
    {
        return $this->hasMany(Picture::class);
    }
    
    public function slideshows ()
    {
        return $this->hasMany(Slideshow::class);
    }
}
