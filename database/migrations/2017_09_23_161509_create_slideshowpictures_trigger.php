<?php

use Illuminate\Database\Migrations\Migration;

class CreateSlideshowpicturesTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        Schema::table('slideshows', function ($table)
        {
            $table->boolean('generated')->default(false);
        });
        DB::getPdo()->exec('
        CREATE TRIGGER tr_slideshow_picture_changes_reset_generation_status AFTER INSERT ON `slideshow_pictures` FOR EACH ROW
                BEGIN
                   UPDATE `slideshows` AS sl SET sl.generated = false WHERE sl.id = NEW.slideshow_id;
                END');
        // INSERT INTO `user_role` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES (3, NEW.id, now(), null);
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        DB::getPdo()->exec('DROP TRIGGER `tr_slideshow_picture_changes_reset_generation_status`');
        Schema::table('slideshows', function ($table)
        {
            $table->dropColumn('generated');
        });
    }
}
