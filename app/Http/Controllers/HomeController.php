<?php

namespace App\Http\Controllers;

use App\Occasion;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct ()
    {
        //$this->middleware('auth');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index ()
    {
        $occasions = Occasion::with(['pictures','slideshows'])->get();
        
        return view('home', compact('occasions'));
    }
    
    public function folder ($folder)
    {
        $occasions = Storage::disk('local')->directories('public/pictures/' . $folder);
        for ($i = 0; $i < sizeof($occasions); $i++)
        {
            $tmp           = explode("/", $occasions[$i]);
            $occasions[$i] = end($tmp);
        }
        
        return view('pics.folder', compact('occasions', 'folder'));
    }
    
    public function occasion ($folder, $occ)
    {
        dd($folder, $occ);
    }
}
