<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlideshowPicture extends Model
{
    protected $fillable = [ 'slideshow_id', 'picture_id', 'order', 'duration' ];
    
    public function picture ()
    {
        return $this->belongsTo(Picture::class);
    }
    
    public function slideshow ()
    {
        return $this->belongsTo(Slideshow::class);
    }
}